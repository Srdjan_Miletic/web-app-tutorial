import mongo_manager from '../utility/mongo_manager';

const dotenv = require('dotenv').config();  // eslint-disable-line
const database_url = process.env.DATABASE_URL;

const express = require('express');
const body_parser = require('body-parser');

const people_router = express.Router();
people_router.use(body_parser.urlencoded({ extended: false }));

people_router.route('/people')
    .post((req, res) => {
        const people_to_add = JSON.parse(req.body.people);
        mongo_manager.insert_into_collection(database_url, 'people', people_to_add)
            .then((inserted_items) => {
                res.status(201).send(inserted_items);
            })
            .catch(error => res.status(500).send({
                error: `Could not fetch data error = ${error}`
            }));
    })
    .delete((req, res) => {
        res.status(405).send();
    })
    .get((req, res) => {
        mongo_manager.get_all_from_collection(database_url, 'people')
            .then(people => res.send(people))
            .catch(error => res.status(500).send({
                error: `Could not fetch data error = ${error}`
            }));
    });

export default people_router;
