const mongo_db_client = require('mongodb').MongoClient;

function get_all_from_collection(database_url, collection_name) {
    return new Promise((resolve, reject) => {
        mongo_db_client.connect(database_url)
            .then((database) => {
                const collection = database.collection(collection_name);
                const result = collection.find({}, { _id: false }).toArray();
                database.close();
                return result;
            })
            .then(items => resolve(items))
            .catch(err => reject(err));
    });
}

function insert_into_collection(database_url, collection_name, array_of_objects_to_add) {
    return new Promise((resolve, reject) => {
        mongo_db_client.connect(database_url)
            .then((database) => {
                const collection = database.collection(collection_name);
                const result = collection.insertMany(array_of_objects_to_add);
                database.close();
                return result;
            })
            .then(inserted_items => resolve(inserted_items))
            .catch(err => reject(err));
    });
}

export default {
    get_all_from_collection,
    insert_into_collection
};
