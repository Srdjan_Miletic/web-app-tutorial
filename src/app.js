import people_router from './routes/people';

const dotenv = require('dotenv').config();  // eslint-disable-line
const port = process.env.PORT || 3000;

const express = require('express');

const app = express();
app.use('', people_router);

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
