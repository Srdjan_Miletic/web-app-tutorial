import 'babel-polyfill';
import mongo_manager from '../../src/utility/mongo_manager';

const should = require('chai').should();
const mongo_db_client = require('mongodb').MongoClient;

const test_database_url = 'mongodb://127.0.0.1:27017/test_database';
const test_data = [{ name: 'test_user_1' }, { name: 'test_user_2' }, { name: 'test_user_3' }];

describe('Database Tests', () => {
    beforeEach('Reset Test Database', () => {
        let db;
        return mongo_db_client.connect(test_database_url)
            .then((test_database) => {
                db = test_database;
            })
            .then(() => db.collections()
                .then((collections) => {
                    if (collections.some(collection => collection.s.name === 'people')) {
                        return db.collection('people').drop();
                    }
                    return Promise.resolve();
                })
                .catch(err => Promise.reject(new Error(err))))
            .then(() => db.collection('people').insertMany(test_data))
            .then(() => db.close())
            .catch(error => console.log(`Error in database tests: ${error}`));
    });

    it('should get all from collection', () => mongo_manager.get_all_from_collection(test_database_url, 'people')
        .then((people) => {
            people.should.have.lengthOf(3);
            people.every((person, index) => person.name === test_data[index].name);
        }));

    const insert_test_data = [{ name: 'insert_test' }];
    it('should insert into collection', () => mongo_manager.insert_into_collection(test_database_url, 'people', insert_test_data)
        .then(() => mongo_manager.get_all_from_collection(test_database_url, 'people'))
        .then((people) => {
            people.should.have.lengthOf(4);
            people[3].name.should.equal('insert_test');
        }));
});
