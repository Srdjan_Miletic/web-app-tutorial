const path = require('path');
const fs = require('fs');

const node_modules = {};
fs.readdirSync('node_modules')
    .filter(x => ['bin'].indexOf(x) === -1)
    .forEach((module) => {
        node_modules[module] = `commonjs ${module}`;
    });

module.exports = {
    entry: './src/app.js',
    target: 'node',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    externals: node_modules,
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['es2015']
                }
            }
        }
        ]
    }
};
