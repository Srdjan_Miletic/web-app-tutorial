module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "camelcase": [0],
        "comma-dangle": ["error", "never"],
        "linebreak-style": "off",
        "no-unused-vars": [
            "error",
            {
                "varsIgnorePattern": "should|expect"
            }
        ],
        "indent": [2, 4]
    },
    "env":{
        "node": true,
        "mocha": true
    }
};